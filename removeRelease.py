#!/usr/bin/python

import argparse

from releasepy import ReleaseTree

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('-p', '--platform', help="Platform to remove", default='', dest='platform')
    parser.add_argument('-r', '--release-number', help="Release number", default='auto', dest='releasever')
    parser.add_argument('-e', '--endsystem', help="installation check in CVMFS or AFS", default='AFS', dest='endsystem')
    parser.add_argument('-l', '--list', help="List of packages to remove", default=[], nargs='+', type=str, dest='removelist')

    args = parser.parse_args()

    print "Starting installation check..."
    print "Command: "
    for arg in vars(args):
        print arg, getattr(args, arg)

    release = ReleaseTree(args.endsystem, args.releasever)

    if not args.platform:
        raise RuntimeError("Platform to check or -a(--checkall to check all platforms) option must be specified")

    if args.removelist:
        release.removeListPackages(args.platform, args.removelist)
    else:
        release.removeAllPackages(args.platform)

if __name__ == "__main__":
  main()
