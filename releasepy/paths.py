ENDSYSTEMS = ['afs', 'cvmfs']

RELEASE_TARS = 'http://lcgpackages.web.cern.ch/lcgpackages/tarFiles/releases/'

RELEASE_READ = {
    'afs': '/afs/cern.ch/sw/lcg/releases',
    'cvmfs': '/cvmfs/sft.cern.ch/lcg/releases'
}

RELEASE_WRITE = {
    'afs': '/afs/.cern.ch/sw/lcg/releases',
    'cvmfs': '/cvmfs/sft.cern.ch/lcg/releases'
}

NIGHTLY_READ = {
    'cvmfs': '/cvmfs/sft.cern.ch/lcg/nightlies'
}

NIGHTLY_WRITE = {
    'cvmfs': '/cvmfs/sft.cern.ch/lcg/nightlies'
}

VIEW = {
    'afs': '/afs/cern.ch/sw/lcg/views',
    'cvmfs': '/cvmfs/sft.cern.ch/lcg/views'
}

GCCPATHS = {
    'afs': '/afs/cern.ch/sw/lcg/contrib/gcc',
    'cvmfs': '/cvmfs/sft.cern.ch/lcg/contrib/gcc/'
}

def release_tarfiles():
    return RELEASE_TARS

def release_readonly(endsystem):
    end = endsystem.lower()
    if end in RELEASE_READ:
        return RELEASE_READ[end]
    else:
        raise RuntimeError("Endsystem {0} not supported for releases in read-only mode".format(end))

def release_write(endsystem):
    end = endsystem.lower()
    if end in RELEASE_WRITE:
        return RELEASE_WRITE[end]
    else:
        raise RuntimeError("Endsystem {0} not supported for releases in write mode".format(end))

def view(endsystem):
    end = endsystem.lower()
    if end in VIEW:
        return VIEW[end]
    else:
        raise RuntimeError("Endsystem {0} not supported for views".format(end))

def nightlies_readonly(endsystem):
    end = endsystem.lower()
    if end in NIGHTLY_READ:
        return NIGHTLY_READ[end]
    else:
        raise RuntimeError("Endsystem {0} not supported for nightlies".format(end))

def nightlies_write(endsystem):
    end = endsystem.lower()
    if end in NIGHTLY_WRITE:
        return NIGHTLY_WRITE[end]
    else:
        raise RuntimeError("Endsystem {0} not supported for nightlies".format(end))

def gcc(endsystem):
    end = endsystem.lower()
    if end in GCCPATHS:
        return GCCPATHS[end]
    else:
        raise RuntimeError("Endsystem {0} not supported for compilers".format(end))
