#!/usr/bin/env bash
set -e # 'e' quits the script on error, 'x' prints every command for debugging 

###################################################################################
#  This script checks EOS for new versions of NXCALS software and extracts these  #
#  packages to CVMFS if necessary (see JIRA issue SPI-1155). The script must be   #
#  run as user sftnight on a CVMFS release manager in order to access EOS         #
#                                                                                 #
#  Created 2018-12-07 by Johannes Heinz (technical student) on request by SWAN    #
#  Requirements: bash, find, sort, tail, sed, grep, tar, unzip, xrdcp, kinit, ssh #
###################################################################################


# PART I: Copy files from EOS as user 'sftnight' to the Jenkins workspace
###################################################################################

# Setup and initialize EOS connection
export EOS_PATH_NXCALS_SWAN="/eos/project-n/nxcals/swan/nxcals_pro"
export EOS_MGM_URL="root://eosproject-l.cern.ch"

COMMON_OPTIONS="-o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null"
REMOTE="lxplus7.cern.ch"

SSH="ssh -q -x -o BatchMode=yes ${COMMON_OPTIONS} sftnight@${REMOTE}"
SCP="scp -B -C -p ${COMMON_OPTIONS} sftnight@${REMOTE}"

KERBEROS_KEYTAB="/var/spool/cvmfs/sft.cern.ch/sftnight/conf/sftnight.keytab"
kinit sftnight@CERN.CH -5 -V -k -t ${KERBEROS_KEYTAB}
aklog || true


# Check EOS for newest file (Choosing latest alphabetical hit)
function get_filename() {
    # Due to EOS problems on cvmfs-sft.cern.ch (our release manager), we need to scp into lxplus
	# NXCLAS has moved from /eos/project/n/nxcals to /eos/project-n/nxcals which is not mounted on cvfs-sft
    ${SSH} "find ${EOS_PATH_NXCALS_SWAN} -name 'nxcals*${1}*' | sort | tail -n 1"
}

# Extract version number from filename (recognizes only numbers and dots)
function read_version() {
    echo $1 | awk -F"/" '{print $NF}' | grep -Eo '[0-9]+\.(\.[0-9]+|[0-9]+)+'
}

# Check EOS availability
set +e
# Due to EOS problems on cvmfs-sft.cern.ch (our release manager), we need to scp into lxplus
# NXCLAS has moved from /eos/project/n/nxcals to /eos/project-n/nxcals which is not mounted on cvfs-sft
${SSH} "ls ${EOS_PATH_NXCALS_SWAN}/*"
TEST_RESULT=$?
set -e
if [ ${TEST_RESULT} -ne 0 ]; then
    echo "ERROR: Cannot access source folder on EOS"
    exit 1
fi

# Package 1: Java libraries
NXCALS_JAVA="nxcals_java"
NXCALS_JAVA_FILE=$(get_filename lib)
NXCALS_JAVA_VERSION_NEW=$(read_version ${NXCALS_JAVA_FILE})

# Due to EOS problems on cvmfs-sft.cern.ch (our release manager), we need to scp into lxplus
# NXCLAS has moved from /eos/project/n/nxcals to /eos/project-n/nxcals which is not mounted on cvfs-sft
#xrdcp -f ${EOS_MGM_URL}/${NXCALS_JAVA_FILE} ${WORKSPACE}/${NXCALS_JAVA}.tar.gz
${SCP}:${NXCALS_JAVA_FILE} ${WORKSPACE}/${NXCALS_JAVA}.tar.gz

# Package 2: Python module
NXCALS_PYTHON="nxcals_python"
NXCALS_PYTHON_FILE=$(get_filename python)
NXCALS_PYTHON_VERSION_NEW=$(read_version ${NXCALS_PYTHON_FILE})

# Due to EOS problems on cvmfs-sft.cern.ch (our release manager), we need to scp into lxplus
# NXCLAS has moved from /eos/project/n/nxcals to /eos/project-n/nxcals which is not mounted on cvfs-sft
#xrdcp -f ${EOS_MGM_URL}/${NXCALS_PYTHON_FILE} ${WORKSPACE}/${NXCALS_PYTHON}.zip
${SCP}:${NXCALS_PYTHON_FILE} ${WORKSPACE}/${NXCALS_PYTHON}.zip

# PART II: Start the CVMFS transaction as user 'cvsft'
# (!) Escape all internal ENVs with a backslash, i.e. '\$?'
# (!) Do not escape the ENVs that are passed from outside, i.e. '$WORKSPACE'
###################################################################################
sudo -i -u cvsft<<EOF
# set -x
shopt -s nocasematch
for iteration in {1..25}
do
    cvmfs_server transaction sft.cern.ch
    if [ \$? -ne 0 ]; then
        if  [[ "\${iteration}" == "25" ]]; then
            echo "Too many tries. Aborting ... "
            exit 1
        else
            echo "Another CVMFS transaction is already open."
            echo "Going to sleep for 5 minutes ..."
            echo ""
            sleep 5m
        fi
    else
        # Transaction was opened successfully
        break
    fi
done

# Pass all ENVs manually since they are executed by a different user
#-------------------------------------------------------------------
LCG_RELEASE="${LCG_RELEASE}" \
PLATFORM="${PLATFORM}" \
WORKSPACE="${WORKSPACE}" \
DEBUG_SCRIPT="${DEBUG_SCRIPT}" \
NXCALS_JAVA="${NXCALS_JAVA}" \
NXCALS_JAVA_VERSION_NEW="${NXCALS_JAVA_VERSION_NEW}" \
NXCALS_PYTHON="${NXCALS_PYTHON}" \
NXCALS_PYTHON_VERSION_NEW="${NXCALS_PYTHON_VERSION_NEW}" \
${WORKSPACE}/lcgjenkins/nxcals_view_update.sh

UPDATE_RESULT=\$?
if [ \$UPDATE_RESULT -eq 0 ]; then
    echo "The creation of the NXCALS view has worked"
    echo "Publishing CVMFS transaction ..."
    echo
    cd \$HOME
    cvmfs_server publish sft.cern.ch
elif [ \$UPDATE_RESULT -eq 5 ]; then
    echo "The creation of the NXCALS view was cancelled since there are no updates."
    echo "Aborting CVMFS transaction ..."
    echo
    cd \$HOME
    cvmfs_server abort -f sft.cern.ch
else
    echo "ERROR: The creation of the NXCALS view has failed due to errors."
    echo "       Aborting CVMFS transaction ..."
    echo
    cd \$HOME
    cvmfs_server abort -f sft.cern.ch
    exit 1
fi
EOF

# PART III: Test the new view and clean up
###################################################################################

# Clean up after the build
rm -rf ${WORKSPACE}/${NXCALS_JAVA}.tar.gz
rm -rf ${WORKSPACE}/${NXCALS_PYTHON}.zip

# Test the new NXCALS view in on a fitting server with the correct OS
if [[ "${PLATFORM}" != *"centos7"* ]]; then
    echo "ERROR: This is not a CentOS 7 platform."
    echo "       SKIPPING TESTS - ONLY AVAILABLE FOR CENTOS7."
else
    TEST_FOLDER="${HOME}/nxcals_view_test"
    TEST_FILE="${TEST_FOLDER}/nxcals_view_test.sh"
    
    echo "Waiting 4 minutes for the changes to propagate to CVMFS Stratum 1 before starting the tests ..."
    sleep 240
    
    echo
    echo "------------------------------------------------------------------------------------------"
    echo "Testing the new view on ${REMOTE}" 
    echo "source /cvmfs/sft.cern.ch/lcg/views/LCG_${LCG_RELEASE}_nxcals/${PLATFORM}/setup.sh"
    echo "------------------------------------------------------------------------------------------"
    
    # Use shared AFS home directory of sftnight to execute test on remote test server
    mkdir -p ${TEST_FOLDER}
    echo "Shared network area for testing: ${TEST_FILE}"
    echo
    
    cp ${WORKSPACE}/lcgjenkins/nxcals_view_test.sh ${TEST_FILE}
    sed -i "s/ \${TARGET_VIEW}/ \/cvmfs\/sft.cern.ch\/lcg\/views\/LCG_${LCG_RELEASE}_nxcals\/${PLATFORM}/" ${TEST_FILE}
    
    set +e
    ${SSH} ${TEST_FILE}
    TEST_RESULT=$?
    set -e
    
    # Clean up sftnight's test directory on AFS
    rm -rf ${TEST_FOLDER}
    
    if [ $TEST_RESULT -eq 0 ]; then
        echo
        echo "All tests were successful."
        echo
    else
        echo
        echo "ERROR: Some tests failed."
        echo "       You have to fix the view manually since it has already been published to CVMFS"
        echo
        exit 1
    fi
fi
