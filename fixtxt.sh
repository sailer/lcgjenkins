#!/bin/bash

# When, for some reason, the .txt file is not corerct and do not reflects the content of the tarfile
# directory, running this script fixes the .txt file.
# The script has to be run in the dir containing the properties.txt file.
# The script does not take arguments

fin="properties.txt"
if test ! -f $fin; then
    echo "The file propoerties.txt was not found: please re-run in the directory where the file resides"
    exit 1
fi
dtar="docker/build/tarfiles"
ftar="tarfiles.txt"
if test ! -d $dtar; then
    echo "Directory docker/build/tarfiles not found: please re-run in the right directory"
    exit 1
else
    ls -1 docker/build/tarfiles > ftar
fi
fout="properties.txt.fixed"

ret=0
pdeps=''

function getnamehash {
   grout="$1"
   pname=`echo $grout | awk '{split($0,a,","); print a[7]}' | awk '{split($0,b,": "); print b[2]}'`
   # echo "$pname"
   pvers=`echo $grout | awk '{split($0,a,","); print a[8]}' | awk '{split($0,b,": "); print b[2]}'`
   # echo "$pvers"
   phash=`echo $grout | awk '{split($0,a,","); print a[4]}' | awk '{split($0,b,": "); print b[2]}'`
   # echo "$phash"
   namehash="${pname}-${pvers}_${phash}"
#   echo "------------------------------------"
#   echo "---> $namehash"
   return 0

}

rm -fr $fout
while read p; do
   getnamehash "$p"
   grout=`grep "$namehash" $ftar`
   if test ! "x$grout" = "x" ; then
      echo "$p" >> $fout
   fi
done < $fin



