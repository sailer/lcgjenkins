#!/usr/bin/env bash

POSITIONAL=()
while [[ $# -gt 0 ]]
do
    key="$1"
    case $key in
        -i|--install)
            INSTALL=1
            shift
            ;;  
        --help)
            echo "\
./pack_contrib.sh [-i|--install]
    Pack contrib to tarballs. The script needs access to cvmfs/sft.cern.ch and eos to work corectly.
    For now, the list of compilers that are taken is static and hardcoded - if you need to add another compiler, add it
    in script.

    -i|--install - Install directly to eos. If the hash changes, it will ask if override.
"
            exit 0
            ;;
        *)  
            POSITIONAL+=("$1")
            shift
            ;;  
    esac
done
set -- "${POSITIONAL[@]}"

contrib="/cvmfs/sft.cern.ch/lcg/contrib"
eoscontrib="/eos/project/l/lcg/www/lcgpackages/docker/contrib"

#System dependent software
declare -a COMPILERS=("gcc/6.2binutils" "gcc/7binutils" "binutils/2.28" "llvm/6.0.0binutils" )
for COMPILER in ${COMPILERS[@]}; do
  for OS in "slc6" "centos7"; do
    echo "Tar $COMPILER $OS"
    tarname=$(echo $COMPILER | sed 's:[/\.]::g')-$OS.tar
    if [ ! -e $tarname ]; then
        tar -chf $tarname -C $contrib "$COMPILER/x86_64-$OS"
    fi  
    if [ -n "$INSTALL" ]; then
        if [ -e "$eoscontrib/$tarname" ]; then
            echo "$tarname already exists, checking hashes"
            if [ "$(md5sum $eoscontrib/$tarname | awk '{print $1}')" = "$(md5sum $tarname | awk '{print $1}')" ]; then
                echo "Hashes are the same, skiping"
            else
                echo "Tar has changed!"
                read -p "Would you like to override?" -n 1 -r
                echo
                if [[ $REPLY =~ ^[Yy]$ ]]
                then
                    cp ./$tarname $eoscontrib
                fi  
            fi  
        else
            cp ./$tarname $eoscontrib
            echo "$tarname installed to eos"
        fi  
    fi  
  done
done

