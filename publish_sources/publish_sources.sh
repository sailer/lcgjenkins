#!/usr/bin/env bash

# Download generator sources for a specific release, untar and patch them.
# Quality of life feature for experiments who wish to debug using the source code https://sft.its.cern.ch/jira/browse/SPI-1224

set -e           # Fail script on first error

TODAY="$(date +%a)"

# Check environment variables, exit if they are not set
# Expecting values like REPOSITORY=sft.cern.ch, TODAY=Mon, LCG_VERSION=LCG_88, PLATFORM=x86_64-centos7-gcc62-opt
echo
echo "------------------------------------------------------------------------------------------"
echo "REPOSITORY:  ${REPOSITORY:?You need to set REPOSITORY (non-empty)}"
echo "TODAY:       ${TODAY:?You need to set TODAY (non-empty)}"
echo "LCG_VERSION: ${LCG_VERSION:?You need to set LCG_VERSION (non-empty)}"
echo "PLATFORM:    ${PLATFORM:?You need to set PLATFORM (non-empty)}"
echo "You can set a URL filter such as 'MCGenerator' by defining 'PKG_FILTER'"
echo "------------------------------------------------------------------------------------------"
echo

HERE=$(pwd)
WORKSPACE="/tmp/lcg_publish_sources"
SOURCE_LIST="${WORKSPACE}/source_list.txt"
CVMFS_BASE="/cvmfs/${REPOSITORY}/lcg"
mkdir -p ${WORKSPACE}

export EOS_MGM_URL="root://eosproject-l.cern.ch"

# Check the given CVMFS repository
if [[ ! "${REPOSITORY}" =~ ^sft(-nightlies)?.cern.ch$ ]]
then
    echo
    echo "[ERROR] ${REPOSITORY} is an invalid CVMFS repository."
    exit 1
fi

# Check if the CVMFS repository on this machine is currently in a transaction
function repository_in_transaction {
    cvmfs_server list | grep "${REPOSITORY}" | grep "in transaction" > /dev/null;
}

# Define a fallback function to make sure we don't the transaction open
function cancel_transaction {
    echo
    echo "[ERROR] Canceling publish_sources.sh unexpectedly"

    if repository_in_transaction
    then
        echo
        echo "[WARNING] This script was in a CVMFS transaction. Aborting ..."
        cvmfs_server abort -f ${REPOSITORY}
    else
        echo
        echo "[INFO] This script was not in a transaction."
    fi

    exit 1
}

# Register the function cancel_transaction to the ERR signal (called after every failure before exit)
trap cancel_transaction ERR

if repository_in_transaction
then
    echo "[ERROR] ${REPOSITORY} is already in a transaction. Exiting ..."
    exit 1
else
    echo "[INFO] Opening a transaction in ${REPOSITORY} ..."
    cvmfs_server transaction ${REPOSITORY}
fi

# Files are put here
CVMFS_TARGET="${CVMFS_BASE}/sources/${LCG_VERSION}"
mkdir -p ${CVMFS_TARGET}
rm -rf ${CVMFS_TARGET}/*
# Create an empty .cvmfscatalog file as indicators for CVMFS to create a catalog here
touch ${CVMFS_TARGET}/.cvmfscatalog

# Clone directory if it does not already exist
LCGCMAKE_DIR="${WORKSPACE}/lcgcmake"
if [ ! -d "${LCGCMAKE_DIR}" ]; then
    echo "[INFO] Cloning lcgcmake, branch: ${LCG_VERSION}"
    git clone --branch ${LCG_VERSION} "https://gitlab.cern.ch/sft/lcgcmake.git" ${LCGCMAKE_DIR}
fi

# Download the buildinfo file for this LCG version
\curl --silent --fail "https://lcgpackages.web.cern.ch/lcgpackages/source_buildinfo/source_list_${LCG_VERSION}.txt" > ${SOURCE_LIST}

# Temporary dir for downloads, required for file type check
PKG_DIR="${WORKSPACE}/packages"
echo "[INFO] Starting source file processing"
mkdir -p ${PKG_DIR}
cd ${PKG_DIR}

# Download all sources, extract and patch
while read line
do
    # Skip if this does not match the filter
    $(echo ${line} | grep --quiet "${PKG_FILTER}") || continue

    # Extract useful fields from line
    # Expects line of format
    # <pkg_name>-<pkg_ver> http://lcgpackages.web.cern.ch/lcgpackages/tarFiles/sources/MCGeneratorsTarFiles/<pkg_name>-<pkg_ver>.tar.gz TODO: update
    NAME="$(echo ${line} | sed -e 's/.*NAME: //' |  cut -d ',' -f 1 | tr -d '[:space:]')"
    VER="$(echo ${line} | sed -e 's/.*VERSION: //' | cut -d ',' -f 1 | tr -d '[:space:]')"
    URL="$(echo ${line} | sed -e 's/.*SRC: //' | cut -d ',' -f 1 | tr -d '[:space:]')"
    FILENAME="$(echo ${URL} | awk -F "/" '{print $NF}')"

    if [[ ${URL} = "" ]]; then
        echo "[WARNING] Target ${NAME}-${VER} has no URL. Only http-based downloads are supported for now. Skipping."
        continue
    fi

    # Download the source
    set +e
    trap - ERR
    \curl --silent --fail --remote-name "${URL}"
    if [[ $? != 0 ]]; then
        echo "[ERROR] Failed to download ${NAME}-${VER}, attempting to continue."
        continue
    fi
    set -e
    trap cancel_transaction ERR 

    FILE_DIR="${CVMFS_TARGET}/${NAME}/${VER}"

    # Some archives don't have the correct file extension, determine type with 'file' instead
    TYPE=$(file --brief ${FILENAME} | cut -d ' ' -f 1 | tr '[:upper:]' '[:lower:]')
    case $TYPE in
        "gzip")  EXTRACT_CMD="tar --extract --preserve-permissions --gzip --directory ${FILE_DIR} --file ${FILENAME}";;
        "bzip2") EXTRACT_CMD="tar --extract --preserve-permissions --bzip2 --directory ${FILE_DIR} --file ${FILENAME}";;
        "posix") EXTRACT_CMD="tar --extract --preserve-permissions --directory ${FILE_DIR} --file ${FILENAME}";;
        "zip")   EXTRACT_CMD="unzip ${FILENAME} -d ${FILE_DIR}";;
        *)       EXTRACT_CMD="ERROR: Could not figure out how to extract ${FILENAME}";;
    esac
 

    echo "[INFO] Deploying: ${NAME}-${VER} (${FILENAME})"
    mkdir -p "${FILE_DIR}"

    # Extract
    set +e
    trap - ERR
    ${EXTRACT_CMD}
    if [[ $? != 0 ]]; then
        echo "[ERROR] Failed to extract ${FILENAME}, attempting to continue."
        continue
    fi
    set -e
    trap cancel_transaction ERR

    # Make sure that the source files start here, and are not put one dir further down
    while [[ $(find ${FILE_DIR} -mindepth 1 -maxdepth 1 | wc -l) -eq 1 && -d $(find ${FILE_DIR} -mindepth 1 -maxdepth 1) ]]; do
        EXTRA_DIR=$(basename ${FILE_DIR}/*)
        mv ${FILE_DIR}/${EXTRA_DIR}/* ${FILE_DIR}/
        if [[ $(ls -A ${FILE_DIR}/${EXTRA_DIR}) ]]; then
            mv ${FILE_DIR}/${EXTRA_DIR}/.[!.]* ${FILE_DIR}/
        fi
        rmdir ${FILE_DIR}/${EXTRA_DIR}
    done

    # Check for patches, and apply if one exists
    set +e
    trap - ERR
    PATCH_FILE=$(find ${LCGCMAKE_DIR} -name "${NAME}-${VER}.patch")
    if [[ PATCH_FILE != "" && -f ${PATCH_FILE} ]]; then
        echo "[INFO] Found patch ${PATCH_FILE}, applying patch for ${NAME}-${VER}"
        patch --strip=0 --unified --silent --force --ignore-whitespace --input=${PATCH_FILE} --directory=${FILE_DIR}
        if [[ $? != 0 ]]; then
            echo "[ERROR] Failed to apply patch for ${NAME}-${VER}, attempting to continue."
        fi
    fi
    set -e
    trap cancel_transaction ERR

done < "${SOURCE_LIST}"

echo "[INFO] Publishing changes to ${REPOSITORY} ..."
cvmfs_server publish ${REPOSITORY}

# Cleanup
cd ${HERE}
echo "Cleaning up"
rm -rf ${PKG_DIR}
rm -rf ${LCGCMAKE_DIR}
rm -rf ${BUILD_DIR}
rm -fv ${SOURCE_LIST}
rm -rf ${WORKSPACE}

exit 0
