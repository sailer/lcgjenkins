#!/usr/bin/python

import os
import sys
import argparse
import subprocess
import glob
import shutil
import re

RELEASEPATHS = {'afs': '/afs/cern.ch/sw/lcg/releases', 'cvmfs': '/cvmfs/sft.cern.ch/lcg/releases'}

class GarbageCollectorProcess:
    def __init__(self, endsystem='afs'):
        self.basepath = RELEASEPATHS[endsystem.lower()]
        self.removefile = open('unusedPackages.txt', 'w+')
        self.skipList = [x.split("/")[-1] for x in glob.glob(self.basepath+"/LCG*")] + \
            ["MCGenerators", "Grid", "var", "gcc"]

    def closeFile(self):
        self.removefile.close()

    def writeFile(self, msg, reason="[no given reason]"):
        self.removefile.write("{0} # Reason: {1}\n".format(msg, reason))
        self.removefile.flush()

    def getRemoveFileName(self):
        return self.removefile.name

    def searchLinks(self, toPath, fromPath):
        # Look for links of this package from every possible LCG_version release to searchPath
        expandedPaths = fromPath
        existingLinks = []
        for p in expandedPaths:
            realpath = os.path.realpath(p)
            print "real: ", realpath
            print "searched: ", toPath
            if realpath == toPath:
                #print "Link exists from:",os.path.join(p,path)
                existingLinks.append(p)
        print "There are {0} links pointing to: {1}\n{2}".format(len(existingLinks),
                    toPath, "\n\t".join(existingLinks))
        return existingLinks

    def checkPackages(self, subdir=""):
        rootReleasePath = os.path.join(self.basepath, subdir)
        regexStr=r'(x86_64|i686)-(.*)'
        for pkg in os.listdir(rootReleasePath):
            if pkg in self.skipList:
                print ">>>>JUMP: ", pkg
                continue
            pkgPath = os.path.join(rootReleasePath, pkg)
            if os.path.isfile(pkgPath):
                continue
            versions = os.listdir(pkgPath)
            if not versions:
                self.writeFile(pkgPath, reason="Package directory is empty")
            else:
                for versionHash in versions:
                    version = versionHash.split("-")[0]
                    versionPath = os.path.join(rootReleasePath,pkgPath,versionHash)
                    if os.path.isfile(versionPath):
                        continue
                    platforms = os.listdir(versionPath)
                    if not platforms:
                        self.writeFile(os.path.join(pkgPath, versionHash), reason="Version directory is empty")
                    else:
                        for plt in platforms:
                            if not re.match(regexStr,plt): continue
                            pltPath = os.path.join(versionPath,plt)
                            print "Searching link names poiting to: ",pltPath
                            if subdir:
                                allReleases = glob.glob(self.basepath+"/LCG*/{0}/{1}/{2}/{3}".format(subdir, pkg, version, plt))
                            else:
                                allReleases = glob.glob(self.basepath+"/LCG*/{0}/{1}/{2}".format(pkg, version, plt))
                            # limit scan to possible platforms
                            print "candidates: ", "\n\t".join(allReleases)
                            realPlts = [p for p in allReleases if re.match(regexStr,p.split("/")[-1])]
                            existingLinks =  self.searchLinks(toPath=pltPath,fromPath=realPlts)
                            if not existingLinks:
                                self.writeFile(os.path.join(versionPath, plt), reason="Any link points to this directory")

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('-e', '--endsystem', help="installation check in CVMFS or AFS", default='AFS', dest='endsystem')

    args = parser.parse_args()

    garbageCollectorProcess = GarbageCollectorProcess(endsystem=args.endsystem)
    print "Starting garbage collector in {0}...".format(garbageCollectorProcess.basepath)

    garbageCollectorProcess.checkPackages()
    garbageCollectorProcess.checkPackages(subdir="MCGenerators")
    garbageCollectorProcess.checkPackages(subdir="Grid")
    garbageCollectorProcess.closeFile()

    print "Files to remove saved in: {0}/{1}".format(os.getcwd(),garbageCollectorProcess.getRemoveFileName())

if __name__ == "__main__":
    main()
