#!/bin/bash -x

# This script is intended to be executed in the Jenkins MASTER ONLY

export EOS_MGM_URL=root://eosuser.cern.ch

FILES=*.tgz
weekday=`date +%a`
txtfile=LCG_${LCG_VERSION}_${PLATFORM}.txt
isDone=isDone-${PLATFORM}
isDoneUnstable=isDone-unstable-${PLATFORM}
tarfiles=*-${PLATFORM}.tgz

if [[ ${PLATFORM} == *slc6* || ${PLATFORM} == *cc7* || ${PLATFORM} == *centos7* || ${PLATFORM} == *ubuntu* ]]; then
    kinit sftnight@CERN.CH -5 -V -k -t /ec/conf/sftnight.keytab
fi

if [ "${BUILDMODE}" == "nightly" ]; then
    basespace=/eos/project/l/lcg/www/lcgpackages/tarFiles/nightlies/$LCG_VERSION/$weekday
    localspace=/build/workspace/nightlies-tarfiles/$LCG_VERSION/$weekday
else
    basespace=/eos/project/l/lcg/www/lcgpackages/tarFiles/releases
    localspace=/build/workspace/releases-tarfiles
fi

rm $basespace/$txtfile
cd $localspace

if [ "${BUILDMODE}" == "nightly" ]; then 
    rm $basespace/$isDone
    rm $basespace/$isDoneUnstable
    rm $basespace/$tarfiles
    xrdcp -f $isDone root://eosuser.cern.ch/$basespace/$isDone 
    xrdcp -f $isDoneUnstable root://eosuser.cern.ch/$basespace/$isDoneUnstable
fi

xrdcp -f $txtfile root://eosuser.cern.ch/$basespace/$txtfile

if [ "$(ls -A $localspace)" ]; then
    echo "Take action: tarfiles is not Empty"
    
    if [ -n "$FILES" ]; then
	
	for files in $FILES
	
        do
	    if [ "${BUILDMODE}" == "nightly" ]; then
		xrdcp -f $files root://eosuser.cern.ch/$basespace/$files
	    else
		ls $basespace/$files
		if [ $? == 0 ]; then
		    echo "The file already exists in EOS"
		else
                    xrdcp $files root://eosuser.cern.ch/$basespace/$files
		fi
            fi
	done
    else
    echo "there are no .tgz files, however I will consider this is not an error" 	
    exit 0	
    fi
else
    echo "tarfiles directory in the MASTER is empty. I will assume this is not an error though"
    exit 0
fi


