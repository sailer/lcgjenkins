#!/bin/bash

# This script creates a 'latest' toolchain based on 'dev4' with the given
# versions of 'heptools_version', 'CORAL', 'COOL' and 'ROOT'. 

if [ $# -ge 4 ]; then
    SLOT=$1
    CORAL=$2
    COOL=$3
    ROOT=$4
else
    echo "$0: Expecting 4 arguments: [SLOT tag] [CORAL tag] [COOL tag] [ROOT tag]"
    exit 1
fi

# Define PATHs in 
dev4="${WORKSPACE}/lcgcmake/cmake/toolchain/heptools-${SLOT}.cmake"
dev_base="${WORKSPACE}/lcgcmake/cmake/toolchain/heptools-dev-base.cmake"
if [[ ${SLOT} == *"python3"* ]]; then
  latest="${WORKSPACE}/lcgcmake/cmake/toolchain/heptools-latestpython3.cmake"
else
  latest="${WORKSPACE}/lcgcmake/cmake/toolchain/heptools-latest.cmake"
fi

# Replace version of heptools_version and ROOT
cp ${dev4} ${latest}
sed -i "s/^set(heptools_version.*/set(heptools_version latest)/g" ${latest}
sed -i "s/^LCG_AA_project(ROOT.*/LCG_AA_project(ROOT ${ROOT})/g" ${latest}

# Replace version of CORAL and COOL
sed -i "/LCG_AA_project(CORAL/ s/master/${CORAL}/g" ${dev_base}
sed -i "/LCG_AA_project(COOL/ s/master/${COOL}/g" ${dev_base}
