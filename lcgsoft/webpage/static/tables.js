$(document).ready(function() {
  $('#filter_input').val('');


  $.tablesorter.addParser({
    id: 'date',
    is: function(s) {
      return false;
    },
    format: function(s) {
      return Date.parse(s);
    },
    type: 'numeric'
  });

  $("#releases_table")
    .tablesorter({
      headers: {
        1: {
          sorter: 'date'
        }
      }
    })
    .find('thead .header').unbind('click');

  $("#packages_table")
    .tablesorter()

    .find('thead .header').unbind('click');

  $('#filter_input').on('keyup', e => {
    if (packageManager.filter_str != $('#filter_input').val()) {
      packageManager.filter_str = $('#filter_input').val();
      packageManager.page = 1;
      packageManager.update();
    }
  })
});

function showConfigurations() {
  $('#configurations').show();
  $('#packages').hide();
  $('#configurations_button').addClass('active');
  $('#packages_button').removeClass('active');
}

function showPackages() {
  $('#configurations').hide();
  $('#packages').show();
  $('#packages_button').addClass('active');
  $('#configurations_button').removeClass('active');
}

class PackageManager {

  constructor() {
    this.page = 1;
    this.filter_str = '';
    this.order_by = 'name'
  }

  goToStart() {
    this.page = 1;
    this.update();
  }

  goTo(page) {
    if (this.page != Number(page)) {
      this.page = Number(page);
      this.update();
    }
  }

  goBack() {
    if (this.page > 1) {
      this.page -= 1;
      this.update();
    }
  }

  goForward() {
    if (this.page < this.num_pages) {
      this.page += 1;
      this.update();
    }
  }

  goToEnd() {
      this.page = this.num_pages;
      this.update();
  }

  orderBy(order_by) {
    this.order_by = order_by;
    this.update();
  }

  updatePaginator() {
    $('#paginator li:first-child').removeClass('disabled');
    $('#paginator li:last-child').removeClass('disabled');
    $('li>a').blur();
    if (this.page == 1) {
      $('#paginator li:first-child').addClass('disabled');
    }
    if (this.page == this.num_pages) {
      $('#paginator li:last-child').addClass('di#sabled');
    }

    if (this.num_pages < 5) {
      this.rewritePaginatorPages(1, this.page, this.num_pages);
    } else {
      if (this.page < 3) {
        this.rewritePaginatorPages(1, this.page);
      } else if (this.page > this.num_pages - 2) {
        this.rewritePaginatorPages(this.num_pages - 4, this.page);
      } else {
        this.rewritePaginatorPages(this.page - 2, this.page);
      }
    }
  }

  rewritePaginatorPages(start, active, size=5) {
    for (let i = 0; i < size; i++) {
      $(`#paginator li:nth-child(${i+2}) > a`).text(start + i).show();
      if (start + i == active) {
        $(`#paginator li:nth-child(${i+2})`).addClass('active');
      } else {
        $(`#paginator li:nth-child(${i+2})`).removeClass('active');
      }
    }
    for (let i = size; i < 5; i++) {
      $(`#paginator li:nth-child(${i+2}) > a`).hide();
    }
  }

  update() {
    $('#packages tbody').empty().append('<tr><td colspan=5><div class="loader-block center-block"></div></td></tr>');

    fetch(`/api/packages?filter=${encodeURIComponent(this.filter_str)}&page=${this.page}&order_by=${this.order_by}`, {method: 'GET'})
      .then(body => body.json())
      .then(json => {
        if (json.current_page != this.page || json.filter_str != this.filter_str) {
          return;
        }
        $('#packages tbody').empty();
        this.num_pages = json.num_pages;
        $('#packages tbody').append(json.packages.map(row => `
          <tr>
            <td>
              <font color="#FFFFFF">
                <b>
                  <a href="/pkg/${row.name}/">
                   ${row.name}
                  </a>
                </b>
              </font>
            </td>
            <td>
              ${row.category ? row.category : ''}
            </td>
            <td>
              ${row.language ? row.language : ''}
            </td>
            <td>
              ${row.release ?
                `<b>
                  <a href="/pkgver/${row.name}/${row.release}/">
                    ${row.release}
                  </a>
                </b>` : ''
              }
            </td>
            <td>
              ${row.contacts.map(contact => 
                {
                  if (contact.email.includes("://")) {
                    `<a href="${contact.email}">${contact.name ? contact.name : contact.email}</a>`
                  } else if (contact.email.includes("@")) {
                    `<a href="mailto:${contact.email}">${contact.name ? contact.name : contact.email}</a>`
                  } else {
                    `${contact.name}`
                  }
                }).join(', ')
              }
            </td>
          </tr>
        `))
        this.updatePaginator();
        $("#packages").trigger("update");
        $('#num_pages').text(this.num_pages);
      })
  }
}

const packageManager = new PackageManager();
packageManager.update();
