# Copy this file and name it db_settings.py.
# Fill the needed fields

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'djangodb',
        'USER': 'django',
        'PASSWORD': 'password', # Replace this for the real password
        'HOST': 'dbod-lcgsoft.cern.ch',
        'PORT': '6620',
    }
}
