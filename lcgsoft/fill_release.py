#!/usr/bin/python

import datetime
import time
import os
import sys
import argparse
os.environ['DJANGO_SETTINGS_MODULE'] = 'settings'
from django.core.exceptions import ObjectDoesNotExist
from functional import seq
import json
from relinfo.models import ReleasePackage, TagDependency, ReleaseTag, Package, Tag, Platform, ReleasePlatform, Release, LCG_RELEASE, PACKAGE_CATEGORIES, PACKAGE_LANGUAGES


def read_release_info(filename):
    # if it is a good json, just read it
    if filename.endswith('json'):
        return json.load(open(filename, 'r'))
    else:
        # here we have to parse the file
        pass


def fill_release_into_DB(release, date_input, description_input, output_path):

    # check whether it already exists
    if len(Release.objects.filter(version=release['description']['version'])) != 0:
        print 'WARNING: Release %s already exists' % release['description']['version']
        r_db = Release.objects.filter(
            version=release['description']['version'])[0]

    else:
        r_db = Release(version=release['description']['version'],
                       # description = 'Setting the contains of the LCG_71root6 release',
                       # date = datetime.datetime.today(),# release['description']['date'],
                       description=description_input,
                       date=date_input,
                       type=LCG_RELEASE)

        r_db.save()
    add_platform_to_release(r_db, release)

    if output_path:
        print 'Writing json files'
        refreshPackageInfo(Package.objects.all(), output_path + '/packages.json')
        refreshReleasesInfo(output_path + '/releases.json')
    else:
        print 'Ignoring json files'


def fill_tag_dependencies(release):
    pass


def add_platform_to_release(r_db, release):
    '''add a platform and the corresponding tags'''
    # create platform if not there yet
    platform = release['description']['platform']
    result = Platform.objects.filter(name=platform)
    if len(result) == 0:
        p_db = Platform(name=platform)
        p_db.save()
    else:
        p_db = result[0]
    print 'adding the platform %s' % p_db
    # add platform to release if not already part of it
    result = r_db.platforms.filter(name=platform)
    if len(result) == 0:
        rp_db = ReleasePlatform(release=r_db, platform=p_db)
        rp_db.save()
    # finally start adding the package tags
    add_package(release['packages'], r_db, p_db)
    add_package_to_release(release['packages'], r_db, p_db)
    add_tags_to_release(release['packages'], r_db, p_db)
    add_tag_dependencies(release['packages'], r_db, p_db)


def add_package(packages, r_db, p_db):
    for package, info in packages.iteritems():
        if '-' in package:
            package = package[:-6]
        pkg = Package(name=info['name'])
        try:
            pkg.save()
        except BaseException:
            continue


def add_tags_to_release(packages, r_db, p_db):
    for package, info in packages.iteritems():
        if '-' in package:
            package = package[:-6]
        # TODO: fix in case of empty hash
        full_version = '%s-%s' % (info['version'], info['hash'])
        full_version = info['version']  # full_version[:-6]

        package = Package.objects.get(name=package)
        tag, created = Tag.objects.get_or_create(
            name=full_version, package=package)
        if created:
            print 'created %s of %s' % (full_version, package)
        release_tag, created = ReleaseTag.objects.get_or_create(
            release=r_db, tag=tag, platform=p_db)
        if created:
            print 'Attached tag %s of %s to release.' % (full_version, package)


def add_package_to_release(packages, r_db, p_db):
    for package, info in packages.iteritems():
        package = package[:-6]
        pkg_name = info['name']
        package = Package.objects.get(name=pkg_name)
        release_package, created = ReleasePackage.objects.get_or_create(
            release=r_db, package=package, platform=p_db)
        if created:
            print 'Attached package %s of %s to release.' % (pkg_name, package)


def add_tag_dependencies(packages, r_db, p_db):
    # tags =
    deps = []
    for package, info in packages.iteritems():
        #     try:
        deps = info['dependencies']
        fromtag = getTagByPkgNameAndVersion(info['name'], info['version'])

    for dep in deps:
        for packagen, inf in packages.iteritems():
            full_version = '%s-%s' % (inf['name'], inf['hash'])
            if dep == full_version:
                totag = getTagByPkgNameAndVersion(inf['name'], inf['version'])
                tagr, created = TagDependency.objects.get_or_create(
                    from_tag=fromtag, to_tag=totag, platform=p_db)
                if created:
                    print 'created tagdependency' + str(tagr.id)


def getTagByPkgNameAndVersion(pkgname, pkgversion):
    return Tag.objects.get(package__name=pkgname, name=pkgversion)


def refreshPackageInfo(packages, output_path):
    '''Refreshs the content of the package info file so those without description
    or contacts can be easily found'''

    orderedPackages = sorted(packages, key=lambda p:
                             (p.description != '', p.contacts.count() > 0, p.name))

    objs = seq(packages)\
        .order_by(order_by_key)\
        .map(lambda p: {
            'description': p.description,
            'contacts': seq(p.contacts.all()).map(lambda c: {'name': c.name, 'email': c.email}).list() if p.contacts.count() > 0 else [],
            'name': p.name,
            'fullname': p.fullname,
            'homepage': p.homepage,
            'category': PACKAGE_CATEGORIES[p.category][1] if p.category else None,
            'language': PACKAGE_LANGUAGES[p.language][1] if p.language else None,
            'license': p.license.name if p.license else None,
        }).list()

    f = open(output_path, 'w')
    f.write(json.dumps(objs, sort_keys=True, indent=4))
    f.close()


def refreshReleasesInfo(output_path):
    '''Updates the contents of the releases.json file inside the lcgcmake repository'''
    releases = Release.objects.all().order_by('-version')

    objs = seq(releases)\
        .map(lambda r: {
            'version': r.version,
            'extra_notes': r.extra_notes
        }).list()
    f = open(output_path, 'w')
    f.write(json.dumps(objs, sort_keys=True, indent=4))
    f.close()


def order_by_key(package):
    '''Ordering method for package output. Puts first the packages with the less info'''
    def f(p): return int(bool(p))
    return (f(package.category) + f(package.contacts) + f(package.description)
            + f(package.fullname) + f(package.homepage) + f(package.language) +
            f(package.license) + f(package.name), package.name)


##########################
if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description='Reads the json file generated by release_summary_reader and uploads it to the db. It also generates a json file that can be used to update the packages information.')

    parser.add_argument('filename', help='json file name')
    parser.add_argument('-d', help='date of release YYYY-MM-DD',
                        default=time.strftime('%Y-%m-%d'), dest='date_input')
    parser.add_argument('-e', help='brief explanation of the release',
                        default='', dest='description_input')

    parser.add_argument('--out', help='Output file for the packages file')

    args = parser.parse_args()

    if (os.path.exists(args.filename)):
        release = read_release_info(args.filename)

        fill_release_into_DB(release, args.date_input,
                             args.description_input, args.out)
    else:
        print 'The file does not exist'
