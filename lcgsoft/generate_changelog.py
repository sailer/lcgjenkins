#!/usr/bin/python

import argparse
import os
os.environ['DJANGO_SETTINGS_MODULE'] = 'settings'
from functional import seq
import json
from relinfo.models import Release, PACKAGE_CATEGORIES
from django.db import connection


def generate_changelog(old_version, new_version):
    cursor = connection.cursor()
    cursor.execute("""SELECT COALESCE(p1.category, p2.category, 8) AS category, COALESCE(p1.name, p2.name) AS name, p1.tag, p2.tag
                      FROM (SELECT DISTINCT p.category, p.id, p.name, array_agg(DISTINCT t.name) AS tag
                            FROM relinfo_release r
                            INNER JOIN relinfo_releasetag rt ON r.id = rt.release_id
                            INNER JOIN relinfo_tag t ON rt.tag_id = t.id
                            INNER JOIN relinfo_package p ON t.package_id = p.id
                            WHERE r.version = %s
                            GROUP BY p.id, p.name) p1
                      FULL OUTER JOIN (SELECT DISTINCT p.category, p.id, p.name, array_agg(DISTINCT t.name) AS tag
                                       FROM relinfo_release r
                                       INNER JOIN relinfo_releasetag rt ON r.id = rt.release_id
                                       INNER JOIN relinfo_tag t ON rt.tag_id = t.id
                                       INNER JOIN relinfo_package p ON t.package_id = p.id
                                       WHERE r.version = %s
                                       GROUP BY p.id, p.name) p2 ON p2.id = p1.id
                      WHERE p1.tag <> p2.tag OR p1.tag IS NULL OR p2.tag IS NULL
                      ORDER BY category, name;""", [old_version, new_version])

    packagesByCategory = seq(cursor.fetchall())\
        .map(lambda x: (x[0], [x[1], set(x[2] or []) - set(x[3] or []), set(x[3] or []) - set(x[2] or [])]))\
        .group_by_key()\
        .dict()

    md = '# Version %s to %s changelog\n\n' % (old_version, new_version)

    for cat in [(None, u'Without category')] + list(PACKAGE_CATEGORIES):
        if cat[0] in packagesByCategory.keys():
            md += '\n## %s\n\n' % cat[1]
            md += seq(packagesByCategory[cat[0]])\
                .map(row_to_md).reduce(lambda x, y: x + '\n' + y)
            md += '\n'
    return md


def row_to_md(row):
    if not row[1]:
        ret = '* New package: '
        ret += ', '.join(map(lambda x: row[0] + '-' + x, row[2]))
        return ret
    elif not row[2]:
        ret = '* Removed package: '
        ret += ', '.join(map(lambda x: row[0] + '-' + x, row[1]))
        return ret
    else:
        ret = '* %s: ' % (row[0])
        ret += ', '.join(map(lambda x: row[0] + '-' + x, row[1]))
        ret += ' => '
        ret += ', '.join(map(lambda x: row[0] + '-' + x, row[2]))
        return ret


if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description='Generates a changelog file between two different releases.')
    parser.add_argument('old_version', nargs='?', type=str, help='Old release')
    parser.add_argument('new_version', nargs='?', type=str, help='New release')

    parser.add_argument('--save', action='store_true',
                        help="Save the date in the database")
    parser.add_argument('--out', type=str,
                        help='Output file')

    args = parser.parse_args()

    if not args.old_version or not args.new_version:
        # <%> Symbols must be double in order to escape them
        releases = Release.objects.raw('''
            SELECT * FROM relinfo_release
            WHERE version !~ 'dev3' AND version !~ 'dev4' AND
                  version !~* 'python3' AND version !~* 'py3'
            ORDER BY date DESC
            LIMIT 2''')

        new_release = releases[0]

        md = generate_changelog(releases[1].version, releases[0].version)
    else:
        new_release = Release.objects.get(version=args.new_version)

        md = generate_changelog(args.old_version, args.new_version)

    if args.out:
        f = open(args.out, 'w')
        f.write(md)
        f.close()
    elif args.save:
        new_release.release_notes = md
        new_release.save()
