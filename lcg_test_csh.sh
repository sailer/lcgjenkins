#!/bin/bash -x -e

# Taken from Jenkins' configuration for job "lcg_test_archdocker" on 2019-06-19 
# Extended with initial timeout loop to ensure that the latest nightly builds are already present on CVMFS stratum 1

# Wait until today's nightly is available on stratum 1
if [[ "${LCG_VERSION}" == *"dev"* ]]
then
    today=$( date +%a )
    
    for iterations in {1..12}
    do
        latest_day=$( realpath /cvmfs/sft.cern.ch/lcg/views/${LCG_VERSION}/latest/${PLATFORM}/.. | sed "s|/.*/||g" )
        if [[ "${today}" == "${latest_day}" ]]
        then
            echo "[INFO] Latest nightly is available for testing."
            break
        else
            if  [[ "${iterations}" == "12" ]]
            then
                echo "[ERROR] Abort after two hours of waiting."
                exit 1
            else
                echo "[WARNING] Nightly is not yet present on CVMFS stratum 1. Going to sleep for 10 minutes ..."
                sleep 10m
            fi
        fi
    done
fi

export WORKSPACE_HOST=$WORKSPACE
export WORKSPACE='/build/jenkins/workspace'
touch $WORKSPACE_HOST/controlfile

# Docker command
# -w directory                  Working directory inside the container
# -e variable=value             Environment variable to define inside the container 
#    LCG_VERSION=$LCG_VERSION               -> Needed inside cmake/ctest commands
#    LCG_IGNORE=$LCG_IGNORE                 -> Needed inside cmake/ctest commands
#    BUILDMODE=$BUILDMODE                   -> Needed inside cmake/ctest commands
#    LCG_INSTALL_PREFIX=$LCG_INSTALL_PREFIX -> Needed inside cmake/ctest commands
#    LCG_EXTRA_OPTIONS=$LCG_EXTRA_OPTIONS   -> Needed inside cmake/ctest commands  
#    TARGET=$TARGET                         -> Needed inside cmake/ctest commands
#    TEST_LABELS=$TEST_LABELS               -> Needed by tests, it determines kind of tests to run
#    USER=sftnight          -> Needed by some CORAL tests which rely on this env variable 
#    GIT_COMMIT=$GIT_COMMIT -> Needed to avoid repeating update command inside container (otherwise update status appears as failed in cdash)
#    SHELL=$SHELL           -> Needed in some package install instructions using $ENV{SHELL}
# -u username                   User to run all build instructions
# -v host_path:container_path   Folder to bind from docker host to docker container
# gitlab-registry.cern.ch/sft/docker:cc7 image and tag to run in the docker container
# command to execute inside the container (here: script with build instructions followed by params)

case "$LABEL" in
    lcg_docker_cc7 )
        DOCKER_IMAGE=lcg-cc7
        ;;
    lcg_docker_slc6 )  
        DOCKER_IMAGE=lcg-slc6
        ;;
    lcg_docker_ubuntu16 )
        DOCKER_IMAGE=lcg-ubuntu16
        ;;
    lcg_docker_ubuntu18 )
        DOCKER_IMAGE=lcg-ubuntu18
        ;;
    lcg_docker_ubuntu20 )
        DOCKER_IMAGE=lcg-ubuntu20
        ;;
    lcg_docker_fedora30 )
        DOCKER_IMAGE=lcg-fedora30
        ;;
    lcg_docker_fedora31 )
        DOCKER_IMAGE=lcg-fedora31
        ;;
    docker-thin )
        DOCKER_IMAGE=cc7-thin
        ;;
    * )
        echo "Docker image $DOCKER_IMAGE not configured (LABEL: $LABEL)"
        ;;
esac

if [[ "$CTEST_TIMESTAMP" ]] && [[ "$CTEST_TAG" ]]
then
    export NAME=`echo $BUILD_TAG | tr "," "-" | tr "=" "-"`
    
    TOTALCPU=`nproc --all`
    CONTAINERS_LIMIT=$(($EXECUTOR_NUMBER+1))

    if [ -z $DOCKER_CPUS ]; then
        DOCKER_CPUS=$(($TOTALCPU/$CONTAINERS_LIMIT))
    fi

    USER_ID=$(id $(whoami) -u)
    GROUP_ID=$(id $(whoami) -g)

    cp lcgjenkins/macros.cmake lcg-csh-test/macros.cmake
    cat > $WORKSPACE_HOST/lcg-csh-test/TAG << EOF
$CTEST_TIMESTAMP
$MODE
$CTEST_TAG
EOF
    docker pull gitlab-registry.cern.ch/sft/docker:$DOCKER_IMAGE
    docker run -e WORKSPACE="$WORKSPACE" \
               -e LCG_VERSION="$LCG_VERSION" \
               -e BUILDMODE="$BUILDMODE" \
               -e LCG_INSTALL_PREFIX="$LCG_INSTALL_PREFIX" \
               -e LCG_EXTRA_OPTIONS="$LCG_EXTRA_OPTIONS" \
               -e TARGET="$TARGET" \
               -e TEST_LABELS="$TEST_LABELS" \
               -e USER=sftnight \
               -e SHELL="$SHELL" \
               -e GIT_COMMIT="$GIT_COMMIT" \
               -e PROPERTIES_PATH=/lcgjenkins \
               -e BUILDHOSTNAME="$BUILDHOSTNAME" \
               -e PLATFORM="$PLATFORM" \
               -e MODE="$MODE" \
               -u sftnight \
               --name "$NAME" \
               --hostname "$HOSTNAME-docker" \
               --cpus="$DOCKER_CPUS" \
               -v /ccache:/ccache \
               -v /ec/conf:/ec/conf \
               -v /cvmfs:/cvmfs \
               -v $PWD/lcgjenkins:/lcgjenkins \
               -v $PWD/lcg-csh-test:/lcg-csh-test \
                   gitlab-registry.cern.ch/sft/docker:$DOCKER_IMAGE \
                   /bin/bash -x /lcgjenkins/runtestcsh-docker.sh "$VIEW"
else
    echo "No TAG file in test_build/Testing - probable problems:"
    echo "1. CTEST_TIMESTAMP and CTEST_TAG not passed as parameters"
    echo "2. Could not create test_build/Testing/TAG file"
fi
